#!/usr/bin/env ruby

require_relative '../lib/tokumx-curator'
require 'docopt'

doc = <<DOCOPT

Usage:
  #{File.basename(__FILE__)} -h | --help
  #{File.basename(__FILE__)} --partitioned_collection (--host=<host> | --replset=<replset> | --cluster=<cluster>) --db=<db> --collection=<collection> --action=<create_partitioned_collection> --key=<key>
  #{File.basename(__FILE__)} --partitioned_collection (--host=<host> | --replset=<replset> | --cluster=<cluster>) --db=<db> --collection=<collection> --action=<list_partition> [--max=<max>]
  #{File.basename(__FILE__)} --partitioned_collection (--host=<host> | --replset=<replset> | --cluster=<cluster>) --db=<db> --collection=<collection> --action=<add_partition> --newmax=<newmax>
  #{File.basename(__FILE__)} --partitioned_collection (--host=<host> | --replset=<replset> | --cluster=<cluster>) --db=<db> --collection=<collection> --action=<drop_partition> (--max=<max> | --keepfirst=<keepfirst> | --keeplast=<keeplast>)


Options:
  -h --help                   Show this screen.
  --host=<host>               The host to connect to, use for single host connections as HOST:PORT or a mongodb uri: --host localhost:27017 OR --host 'mongodb://[username:password@]host[/database]'
  --replset=<replset>         Use when connecting to a replicaset, either a comma delimited list or a mongodb uri: --replset 'host1:port1,host2:port2' OR --replset 'mongodb://[username:password@]host1[,host2,…[,hostN]][/database]'
  --cluster=<cluster>         Use when connecting to a cluster, either a comma delimited list or a mongodb uri: --replset 'host1:port1,host2:port2' OR --replset 'mongodb://[username:password@]host1[,host2,…[,hostN]][/database]'
  --db=<db>                   Database to use
  --collection=<collection>   Collection to use
  --key=<key>                 Key to use for partitioning, must be a comma delimited list of keys: --key 'time:1,_id:1'
  --max=<max>                 Specifies the maximum partition key for dropping such that all partitions with documents less than or equal to max are dropped: --max 'time:ISODate("2015-04-08T17:00:00Z"),_id:1'
  --newmax=<newmax>           Identifies what the maximum key of the current last partition should become before adding a new partition: --newmax 'time:ISODate("2015-04-08T17:00:00Z"),_id:1'
  --keepfirst=<keepfirst>     Keep the first <keepfirst> partitions, drop the rest
  --keeplast=<keeplast>       Keep the last <keeplast> partitions, drop the rest
  --partitioned_collection    Work on partitioned collections [default: false].
  --action=<action>           The action to execute, available actions are list_partition,create_partitioned_collection,add_partition,drop_partition

DOCOPT

debug = false

begin
  require "pp"
  options = Docopt::docopt(doc)
  pp options if debug
rescue Docopt::Exit => e
  puts e.message
end

exit if options.nil?

if options['--partitioned_collection']
  result = Tokumx::Curator::PartitionedCollection.new(options).run
  puts result.inspect unless options['--action'] == 'create_partitioned_collection'
end
