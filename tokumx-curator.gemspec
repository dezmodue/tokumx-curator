# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'tokumx/curator/version'

Gem::Specification.new do |spec|
  spec.name          = "tokumx-curator"
  spec.version       = Tokumx::Curator::VERSION
  spec.authors       = ["dezmodue"]
  spec.email         = ["dezmodue@gmail.com"]
  spec.summary       = %q{Curate your TokuMX installation}
  spec.description   = %q{Simple gem to help curate some aspects of a TokuMX/MongoDB installation}
  spec.homepage      = 'https://bitbucket.org/dezmodue/tokumx-curator'
  spec.license       = "Apache2"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"

  spec.add_runtime_dependency 'docopt', '~> 0'
  spec.add_runtime_dependency 'mongo', '~> 1.12'
  spec.add_runtime_dependency 'bson', '~> 1.12'
  spec.add_runtime_dependency 'bson_ext', '~> 1.12'
end
