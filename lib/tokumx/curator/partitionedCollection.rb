require 'time'

module Tokumx
  module Curator

    class PartitionedCollection

      include Tokumx::Helpers

      %w(conn host replset cluster db collection action key max newmax keeplast keepfirst).each do |a|
        attr_accessor a.to_sym
      end

      # @param [String] host
      # @param [Hash] args
      # @return [Object]
      def initialize(args={})

        set_instance_vars(args)

        begin
          @conn = get_db_conn
        rescue Exception => e
          raise "Cannot create a connection, #{e.inspect}"
        end

      end

      def run
        self.send(@action)
      end


      # @param [String] key
      # @return [Hash]
      def format_key(key)
        Hash[key.split(',').map {|k| [k.split(':')[0].to_sym, k.split(':')[1].to_i]}]
      end

      # @param [String] max
      # @return [Hash]
      def format_max(max)
        h = Hash[max.split(',').map {|k| [k.split(':',2)[0].to_sym, k.split(':',2)[1]]}]
        h.each do |k,v|
          if is_isodate?(v)
            time = v.match(/^ISODate\((.*)\)/)[1]
            h[k] = DateTime.parse(time).to_time
          elsif is_objectid?(v)
            id = v.match(/ObjectId\("(.*)"\)/)[1]
            h[k] = BSON::ObjectId(id)
          elsif is_int?(v)
            h[k] = v.to_i
          end
        end
      end

      def create_partitioned_collection
        keys = format_key(@key)
        @conn[@db].create_collection(@collection, :primaryKey => keys, :partitioned => true)
      end

      def add_partition
        if partitioned?
          newmax = format_max(@newmax)
          @conn[@db].command({"addPartition" => @collection, "newMax" => newmax })
        end
      end

      def drop_partition
        if @max
          max = format_max(@max)
          @conn[@db].command({ "dropPartition" => @collection, "max" => max })
        elsif @keeplast
          drop_partition_keeplast
        elsif @keepfirst
          drop_partition_keepfirst
        end if partitioned?
      end

      def drop_partition_keeplast
        keep = @keeplast.to_i
        @list = list_partition
        while @list['numPartitions'] > keep
          @conn[@db].command({"dropPartition" => @collection, "id" => @list['partitions'].first['_id'] })
          @list = list_partition
        end
      end

      def drop_partition_keepfirst
        keep = @keepfirst.to_i
        @list = list_partition
        while @list['numPartitions'] > keep
          @conn[@db].command({"dropPartition" => @collection, "id" => @list['partitions'].last['_id'] })
          @list = list_partition
        end
      end

      def list_partition
        if partitioned?
          l = @conn[@db].command({"getPartitionInfo" => @collection })
          if @max
            max = format_max(@max)
            l['partitions'].each do |a|
              @match = a
              max.keys.each do |k|
                @match = nil if max[k] != a['max'][k.to_s]
              end
              break unless @match.nil?
            end
            return @match
          else
            return l
          end
        end
      end

    end
  end
end
