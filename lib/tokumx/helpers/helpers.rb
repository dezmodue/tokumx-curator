
module Tokumx
  module Helpers

    def set_instance_vars(args)
      args.each do |k,v|
        if k.start_with?('--')
          instance_variable_set("@#{k[2..-1]}", v)
        else
          instance_variable_set("@#{k}", v)
        end
      end
    end

    def get_db_conn
      get_replset_conn(@replset) unless @replset.nil?
      get_cluster_conn(@cluster) unless @cluster.nil?
      get_host_conn(@host) unless @host.nil?
    end

    def get_host_conn(host)
      if is_mongodb_uri?(host)
        Mongo::MongoClient.from_uri(host)
      else
        Mongo::MongoClient.new(host.split(':')[0],host.split(':')[1].to_i)
      end
    end

    def get_replset_conn(replset)
      if is_mongodb_uri?(replset)
        Mongo::ReplSetConnection.from_uri(replset)
      else
        Mongo::ReplSetConnection.new(replset.split(','))
      end
    end

    def get_cluster_conn(cluster)
      if is_mongodb_uri?(cluster)
        Mongo::ShardedConnection.from_uri(cluster)
      else
        Mongo::ShardedConnection.new(cluster.split(','))
      end
    end

    def is_mongodb_uri?(s)
      s.is_a?(String) && s.start_with?('mongodb://')
    end

    def partitioned?
      @conn[@db][@collection].stats['partitioned']
    end

    # Transform YYYY-MM-DD to BSON ObjectId
    def date_to_bson(date)
      y,m,d = date.split('-')[0],date.split('-')[1],date.split('-')[2]
      BSON::ObjectId.from_time(Time.new(y,m,d, 00,00,00, '+00:00'))
    end

    # @param [String] s
    # @return [Boolean]
    def is_isodate?(s)
      s.match(/^ISODate\((.*)\)/)
    end

    # @param [String] s
    # @return [Boolean]
    def is_objectid?(s)
      s.match(/ObjectId\("(.*)"\)/)
    end

    # @param [String] s
    # @return [Boolean]
    def is_int?(s)
      s.match(/^[0-9]+$/)
    end
  end
end
