# Tokumx::Curator

Simple gem to help with administrative tasks (TokuMX/MongoDB). Right now it supports managing partitioned collections (TokuMX only - http://docs.tokutek.com/tokumx/tokumx-partitioned-collections.html).
###### Consider it very much a work in progress


# Roadmap
- Reporting for config server differences to help solving out of sync issues
- Manage the balancer: enable, disable, report status (enabled/disabled)
- Move the last chunk to a specific shard in a clustered environment
- Manage db/collection sharding: create sharded collections, report collections sharding status
- Check for empty db/collections

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'tokumx-curator'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install tokumx-curator

## Usage

You can either invoke the executable tokumx-curator.rb or require 'tokumx-curator' in your code.

### CLI Usage

    Usage:
      tokumx-curator.rb -h | --help
      tokumx-curator.rb --partitioned_collection (--host=<host> | --replset=<replset> | --cluster=<cluster>) --db=<db> --collection=<collection> --action=<create_partitioned_collection> --key=<key>
      tokumx-curator.rb --partitioned_collection (--host=<host> | --replset=<replset> | --cluster=<cluster>) --db=<db> --collection=<collection> --action=<list_partition> [--max=<max>]
      tokumx-curator.rb --partitioned_collection (--host=<host> | --replset=<replset> | --cluster=<cluster>) --db=<db> --collection=<collection> --action=<add_partition> --newmax=<newmax>
      tokumx-curator.rb --partitioned_collection (--host=<host> | --replset=<replset> | --cluster=<cluster>) --db=<db> --collection=<collection> --action=<drop_partition> (--max=<max> | --keepfirst=<keepfirst> | --keeplast=<keeplast>)


    Options:
      -h --help                   Show this screen.
      --host=<host>               The host to connect to, use for single host connections as HOST:PORT or a mongodb uri: --host localhost:27017 OR --host 'mongodb://[username:password@]host[/database]'
      --replset=<replset>         Use when connecting to a replicaset, either a comma delimited list or a mongodb uri: --replset 'host1:port1,host2:port2' OR --replset 'mongodb://[username:password@]host1[,host2,…[,hostN]][/database]'
      --cluster=<cluster>         Use when connecting to a cluster, either a comma delimited list or a mongodb uri: --replset 'host1:port1,host2:port2' OR --replset 'mongodb://[username:password@]host1[,host2,…[,hostN]][/database]'
      --db=<db>                   Database to use
      --collection=<collection>   Collection to use
      --key=<key>                 Key to use for partitioning, must be a comma delimited list of keys: --key 'time:1,_id:1'
      --max=<max>                 Specifies the maximum partition key for dropping such that all partitions with documents less than or equal to max are dropped: --max 'time:ISODate("2015-04-08T17:00:00Z"),_id:1'
      --newmax=<newmax>           Identifies what the maximum key of the current last partition should become before adding a new partition: --newmax 'time:ISODate("2015-04-08T17:00:00Z"),_id:1'
      --keepfirst=<keepfirst>     Keep the first <keepfirst> partitions, drop the rest
      --keeplast=<keeplast>       Keep the last <keeplast> partitions, drop the rest
      --partitioned_collection    Work on partitioned collections [default: false].
      --action=<action>           The action to execute, available actions are list_partition,create_partitioned_collection,add_partition,drop_partition


####create_partitioned_collection

Creates a new collection partitioned on the specified key, refer to http://docs.tokutek.com/tokumx/tokumx-partitioned-collections.html#tokumx-partitioned-collections-create for the TokuMX command documentation

    tokumx-curator.rb --host HOST:PORT --partitioned_collection --db partitioned --collection timeISO  --action create_partitioned_collection --key 'ts:1,_id:1'

###### Note that the --key has to end with '_id:1'

#### list_partition
Retrieves the list of partitions for a partitioned collection. Refer to http://docs.tokutek.com/tokumx/tokumx-commands.html#tokumx-commands-getPartitionInfo for the TokuMX command documentation

      tokumx-curator.rb --host HOST:PORT --partitioned_collection --db partitioned --collection timeint  --action list_partition
      
       {"numPartitions"=>3, "partitions"=>[{"_id"=>6, "max"=>{"time"=>2015-04-09 00:00:00 UTC, "_id"=>nil}, "createTime"=>2015-04-08 21:01:00 UTC},{"_id"=>7, "max"=>{"time"=>2015-04-10 00:00:00 UTC, "_id"=>24}, "createTime"=>2015-04-08 21:02:20 UTC}, {"_id"=>8, "max"=>{"time"=>#<BSON::MaxKey:0x007fed238398f8>, "_id"=>#<BSON::MaxKey:0x007fed23839880>}, "createTime"=>2015-04-08 21:19:32 UTC}], "ok"=>1.0}

If you want to check if a specific partition identified by the max key exists, you can pass a specific max key:

    tokumx-curator.rb --host mongodb://HOST:PORT --partitioned_collection --db partitioned --collection timeint  --action list_partition --max 'time:ISODate("2015-04-10T00:00:00Z")'
    
    {"_id"=>7, "max"=>{"time"=>2015-04-10 00:00:00 UTC, "_id"=>24}, "createTime"=>2015-04-08 21:02:20 UTC}

###### Note that we didn't pass a value for \_id but it found a match on the "time" key. If you want an exact match you need to pass all the keys:
    tokumx-curator.rb --host HOST:PORT --partitioned_collection --db partitioned --collection timeint  --action list_partition --max 'time:ISODate("2015-04-10T00:00:00Z"),_id:24'
    
    {"_id"=>7, "max"=>{"time"=>2015-04-10 00:00:00 UTC, "_id"=>24}, "createTime"=>2015-04-08 21:02:20 UTC}
   

####add_partition
Add a partition to a Partitioned Collections, refer to http://docs.tokutek.com/tokumx/tokumx-commands.html#tokumx-commands-addPartition for the TokuMX command documentation

    tokumx-curator.rb --host HOST:PORT --partitioned_collection --db partitioned --collection timeISO  --action add_partition --newmax 'ts:ISODate("2015-04-01T00:00:00Z"),_id:200'
    
    {"ok"=>1.0} 

Now we can list the partitions

    tokumx-curator.rb --host HOST:PORT --partitioned_collection --db partitioned --collection timeISO  --action list_partition
    
    {"numPartitions"=>2, "partitions"=>[{"_id"=>1, "max"=>{"ts"=>2015-04-01 00:00:00 UTC, "_id"=>200}, "createTime"=>2015-04-08 23:27:42 UTC}, {"_id"=>2, "max"=>{"ts"=>#<BSON::MaxKey:0x007f8f4080f260>, "_id"=>#<BSON::MaxKey:0x007f8f4080f198>}, "createTime"=>2015-04-08 23:32:53 UTC}], "ok"=>1.0}"=>1    
    
####drop_partition
Drop all partitions whose max value is less than or equal to the --max argument or by specifying the number of partitions to retain either from the start or the end with --keepfirst or --keeplast arguments, refer to http://docs.tokutek.com/tokumx/tokumx-partitioned-collections.html#tokumx-partitioned-collections-drop for the TokuMX command documentation 

Say that we have a partitioned collection with 5 partitions:

    tokumx-curator.rb --host HOST:PORT --partitioned_collection --db partitioned --collection dropcoll  --action list_partition
    
    {"numPartitions"=>5, "partitions"=>[{"_id"=>0, "max"=>{"ts"=>2015-01-01 00:00:00 UTC, "_id"=>nil}, "createTime"=>2015-04-09 00:41:25 UTC}, {"_id"=>1, "max"=>{"ts"=>2015-01-02 00:00:00 UTC, "_id"=>nil}, "createTime"=>2015-04-09 00:42:07 UTC}, {"_id"=>2, "max"=>{"ts"=>2015-01-03 00:00:00 UTC, "_id"=>nil}, "createTime"=>2015-04-09 00:42:11 UTC}, {"_id"=>3, "max"=>{"ts"=>2015-01-04 00:00:00 UTC, "_id"=>nil}, "createTime"=>2015-04-09 00:42:16 UTC}, {"_id"=>4, "max"=>{"ts"=>#<BSON::MaxKey:0x007ffbf20ca670>, "_id"=>#<BSON::MaxKey:0x007ffbf20ca558>}, "createTime"=>2015-04-09 00:42:19 UTC}], "ok"=>1.0}

Drop all the partitions with max key smaller than {"ts"=>2015-01-02 00:00:00 UTC, "_id"=>nil}:

    tokumx-curator.rb --host HOST:PORT --partitioned_collection --db partitioned --collection dropcoll  --action drop_partition --max 'ts:ISODate("2015-01-02T00:00:00.000+00:00")'

    {"ok"=>1.0} 

List the partitions and check the result:

    tokumx-curator.rb --host HOST:PORT --partitioned_collection --db partitioned --collection dropcoll  --action list_partition
    
    {"numPartitions"=>3, "partitions"=>[{"_id"=>2, "max"=>{"ts"=>2015-01-03 00:00:00 UTC, "_id"=>nil}, "createTime"=>2015-04-09 00:42:11 UTC}, {"_id"=>3, "max"=>{"ts"=>2015-01-04 00:00:00 UTC, "_id"=>nil}, "createTime"=>2015-04-09 00:42:16 UTC}, {"_id"=>4, "max"=>{"ts"=>#<BSON::MaxKey:0x007ffa4c07df48>, "_id"=>#<BSON::MaxKey:0x007ffa4c07dea8>}, "createTime"=>2015-04-09 00:42:19 UTC}], "ok"=>1.0}     
    
Drop newer partitions until we have 2 total partitions:

    tokumx-curator.rb --host HOST:PORT --partitioned_collection --db partitioned --collection dropcoll  --action drop_partition --keepfirst 2

List the partitions and check the result:

    tokumx-curator.rb --host HOST:PORT --partitioned_collection --db partitioned --collection dropcoll  --action list_partition
    
    {"numPartitions"=>2, "partitions"=>[{"_id"=>2, "max"=>{"ts"=>2015-01-03 00:00:00 UTC, "_id"=>nil}, "createTime"=>2015-04-09 00:42:11 UTC}, {"_id"=>3, "max"=>{"ts"=>#<BSON::MaxKey:0x007f9a6303f340>, "_id"=>#<BSON::MaxKey:0x007f9a6303f278>}, "createTime"=>2015-04-09 00:42:16 UTC}], "ok"=>1.0}

The last partition has been dropped (_id:4) and we are left with _id:2 and _id:3

Drop older partitions until we have 1 partition only:

    tokumx-curator.rb --host HOST:PORT --partitioned_collection --db partitioned --collection dropcoll  --action drop_partition --keeplast 1

And check the result:

    tokumx-curator.rb --host HOST:PORT --partitioned_collection --db partitioned --collection dropcoll  --action list_partition
    
    {"numPartitions"=>1, "partitions"=>[{"_id"=>3, "max"=>{"ts"=>#<BSON::MaxKey:0x007fd83b810540>, "_id"=>#<BSON::MaxKey:0x007fd83b810478>}, "createTime"=>2015-04-09 00:42:16 UTC}], "ok"=>1.0}
    
We dropped the oldest partition with _id:2               
    
##### Note on the --max --newmax format
The code will try to match if the passed string represents an ISODate, ObjectId, Integer and if no match is found it will consider the value a String.

    tokumx-curator.rb --host HOST:PORT --partitioned_collection --db partitioned --collection mix  --action create_partitioned_collection --key 'ts:1,int:1,oid:1,string:1,_id:1'

    tokumx-curator.rb --host HOST:PORT --partitioned_collection --db partitioned --collection mix  --action add_partition --newmax 'ts:ISODate("2014-01-01T03:01:01.000+03:00"),int:12345,oid:ObjectId("542c2b97bac0595474108b48"),string:"12345"'
    
    {"ok"=>1.0}
    
    
    tokumx-curator.rb --host HOST:PORT --partitioned_collection --db partitioned --collection mix  --action list_partition
    
    {"numPartitions"=>2, "partitions"=>[
    
    {"_id"=>0, "max"=>{"ts"=>2014-01-01 00:01:01 UTC, "int"=>12345, "oid"=>BSON::ObjectId('542c2b97bac0595474108b48'), "string"=>"\"12345\"", "_id"=>nil}, "createTime"=>2015-04-09 00:12:20 UTC}, 
    
    {"_id"=>1, "max"=>{"ts"=>#<BSON::MaxKey:0x007fba4b05e378>, "int"=>#<BSON::MaxKey:0x007fba4b05e288>, "oid"=>#<BSON::MaxKey:0x007fba4b05e148>, "string"=>#<BSON::MaxKey:0x007fba4b05dfe0>, "_id"=>#<BSON::MaxKey:0x007fba4b05dec8>}, "createTime"=>2015-04-09 00:25:15 UTC}], 
    "ok"=>1.0}
       
### Usage in code

    require 'tokumx-curator'
    p = Tokumx::Curator::PartitionedCollection.new()
    p.host = 'HOST:PORT'
    p.db = 'partitioned'
    p.collection = 'dropcoll'
    p.conn = p.get_db_conn
    p.list_partition

Alternatively initialize the class with all the required args:

    require 'tokumx-curator'
    p = Tokumx::Curator::PartitionedCollection.new({'host' => 'localhost:27017', 'db' => 'partitioned', 'collection' => 'dropcoll'})
    p.list_partition

If we initialize the action key we can call the run method:

    require 'tokumx-curator'
    p = Tokumx::Curator::PartitionedCollection.new({'host' => 'localhost:27017', 'db' => 'partitioned', 'collection' => 'dropcoll', 'action' => 'list_partition'})
    p.run
    
Example run
    
    [1] pry(main)> require 'tokumx-curator'
    => true
    
    [2] pry(main)> p = Tokumx::Curator::PartitionedCollection.new({'host' => 'localhost:27017', 'db' => 'partitioned', 'collection' => 'dropcoll', 'action' => 'list_partition'})
    => #<Tokumx::Curator::PartitionedCollection:0x007fec4aa690f8
     @action="list_partition",
     @collection="dropcoll",
     @conn=
      #<Mongo::MongoClient:0x007fec4aa68c20
       @acceptable_latency=15,
       @auths=#<Set: {}>,
       @connect_timeout=30,
       @db_name=nil,
       @host="localhost",
       @id_lock=#<Mutex:0x007fec4aa68a68>,
       @logger=nil,
       @max_bson_size=16777216,
       @max_message_size=48000000,
       @max_wire_version=nil,
       @max_write_batch_size=nil,
       @min_wire_version=nil,
       @mongos=false,
       @op_timeout=20,
       @pool_size=1,
       @pool_timeout=5.0,
       @port=27017,
       @primary=["localhost", 27017],
       @primary_pool=#<Mongo::Pool:0x3ff62553095c @host=localhost @port=27017 @ping_time= 0/1 sockets available up=true>,
       @read=:primary,
       @read_primary=true,
       @slave_ok=nil,
       @socket_class=Mongo::TCPSocket,
       @socket_opts={},
       @ssl=nil,
       @tag_sets=[],
       @unix=false,
       @write_concern={:w=>1}>,
     @db="partitioned",
     @host="localhost:27017">
    
    [3] pry(main)> p.run
    
    => {"numPartitions"=>1, "partitions"=>[{"_id"=>3, "max"=>{"ts"=>#<BSON::MaxKey:0x007fec4b8cfb88>, "_id"=>#<BSON::MaxKey:0x007fec4b8cfa48>}, "createTime"=>2015-04-09 00:42:16 UTC}], "ok"=>1.0}



## Contributing

1. Fork it ( https://github.com/[my-github-username]/tokumx-curator/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
